<?php

/**
 * Implementation of hook_diff() for node.module (body and title).
 */
function node_diff(&$old_node, &$new_node) {
  global $engine;
  
  $result = array();
  $result[] = array(
    'name' => t('Title'),
    'old' => array($old_node->title),
    'new' => array($new_node->title),
    'format' => array(
      'show_header' => false,
    )
  );
    
  $result[] = array(
    'name' => t('Body'),
    'old' => cleanHTMLforDiff($old_node->body),
    'new' => cleanHTMLforDiff($new_node->body),
    //'old' => $engine ? explode("\n", $old_node->body) : array($old_node->body),
    //'new' => $engine ? explode("\n", $new_node->body) : array($new_node->body),
  );
  return $result;  
}


// use this filter to strip HTML but also to ensure we retain LF's
// function is used here as well as in cck.inc
function cleanHTMLforDiff($instring) {
  global $engine;
  
  // remove any /n's since they don't mean anything for html content
  $instring = str_replace("\n", "", $instring);
        
  $instring = preg_replace('^[\x7f-\xff]^', " ", $instring);    // replace non-printable's with spaces - this should be handled in input filter  
  $instring = preg_replace('/\s\s+/', ' ', $instring);
    
  $instring = str_replace(array("<br>", "<br />"), "\n", $instring);    // replace <br>'s with LF's 
  
  switch ($engine) {
    case 0:         // inline
      // handle variations in <p> - ideally this should all be handled as input filter; but for now lets clean here
      $instring = preg_replace('^</p>\s*<p>^', "</p><p>", $instring); 
      $instring = preg_replace('^\s*</p>^', "</p> ", $instring);
      $instring = preg_replace('^<p>\s*^', "<p> ", $instring);    
  
      $instring = strip_tags(html_entity_decode($instring, ENT_QUOTES), "<p>");    //  then strip the rest - but let's leave <p>'s
      break;
    case 1:         // 2-column
      $instring = str_replace("<p>", "\n", $instring);                      // replace <p>'s with LF's - then strip the closing </p>  
      $instring = strip_tags(html_entity_decode($instring, ENT_QUOTES));    //  then strip the rest 
  }
  
  return ($engine ? explode("\n", $instring) : array(str_replace("\n", "<br>", $instring)));
}
