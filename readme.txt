DESCRIPTION
----------------
This module adds 'diff' functionality to the 'Revisions' tab, allowing
users to nicely view all the changes between any two revisions of a node.

This LifeWire version of the Diff module stems from the Drupal.org project "Diff" 
at: http://drupal.org/project/diff. It adds the ability to do Inline revision 
comparison using the PEAR Text_Diff class. The user may select either the Diff 
modules original 2 column compare format or the Inline format in their profile. 

The PEAR class Text_Diff must be installed for this module to work.

INSTALL
----------------
Install as usual for Drupal modules.

TECHNICAL
-------------------
- This version compares the raw data, not the filtered output, making
it easier to see changes to HTML entities, etc.
- The diff engine for 2 column format is a GPL'ed php diff engine from phpwiki.
- The diff engine for the Inline format is from PEAR's Text_Diff.
